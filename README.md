PKPCLI
======

simple shel-like interface to keepass db.

Requirements
------------

* [keepassdb](https://github.com/hozn/keepassdb)

Usage
-----

Simply run `cli.py` (to be renamed) with --help
Once the shell has started, use the `help` command to have a list of available
commands.

Limitations
-----------

A lot.
